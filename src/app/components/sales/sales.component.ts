import { Component, OnInit } from '@angular/core';
import { SalesModel } from 'src/app/model/sales/sales.component';
import { SalesService } from 'src/app/service/sales.service';
import { MatDialog, MatTableDataSource } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
import { UsuarioModel } from 'src/app/model/usuario/usuario.component';
import { Router } from '@angular/router';
import { TableUtil } from '../produto/tableUtils';
import { MessageService } from 'src/app/service/message.service';
import { SaleItensService } from 'src/app/service/sale-itens.service';
import { DialogAlertaComponent } from '../dialog-alerta/dialog-alerta.component';



@Component({
  selector: 'app-sales',
  templateUrl: './sales.component.html',
  styleUrls: ['./sales.component.css']
})

export class SalesComponent implements OnInit {
  dataSource = new MatTableDataSource<SalesModel>(null);
  displayedColumns: string[] = ['select', 'id', 'data', 'idCliente', 'idItens', 'valorTotal',];
  selection = new SelectionModel<SalesModel>(true, []);
  msg: string;

  constructor(private salesItensService: SaleItensService, private messageService: MessageService, private router: Router, private salesService: SalesService, private dialog: MatDialog) { }

  ngOnInit() {
    this.getAll();
    this.getTotal();
  }

  getAll() {
    this.salesService.getAll().subscribe(response => {
      this.dataSource.data = response as SalesModel[];
    },
      err => {
        alert("Ocorreu um erro de conexão com o servidor. Verifique e tente novamente !")
        this.messageService.action = false;
      }
    );
    if (this.dataSource.data.length == 0) {
      alert("Dados inexistentes")
    }
  }

  getTotal() {
    return this.dataSource.data.map(t => t.valorTotal).reduce((acc, value) => acc + value, 0);
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));

  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: SalesModel): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.id + 1}`;

  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  onSaleDetail() {
    if (this.selection.selected.length > 1) {
      this.msg = "Escolha somente um registro !";
      this.message();

    } else {
      this.salesItensService.salesDto = this.selection.selected
      this.selection.selected.forEach(item => {
        this.messageService.id = item.id;
        this.messageService.idItens = item.idItens;
        this.router.navigate(['/sale-itens'])
      });
     
    }
  }

  message() {
    this.messageService.message = this.msg;
    this.openDialogAlerta();
  }

  exportTable() {
    var name: string;
    name = "Vendas"
    TableUtil.exportToExcel("ProdutoTable", name);
  }

  openDialogAlerta() {
    this.messageService.action = false;
    const dialogRef = this.dialog.open(DialogAlertaComponent, { width: '300px', });
    dialogRef.afterClosed().subscribe(result => {
      this.getAll();
      this.selection.clear();
    });

  }
}