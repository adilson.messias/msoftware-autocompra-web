import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ProdutoService } from 'src/app/service/produto.service';
import { ProdutoModel } from 'src/app/model/produto/produto.component';
import { MessageService } from 'src/app/service/message.service';
import { CarrinhoService } from 'src/app/service/carrinho.service';
import { CarrinhoModel } from 'src/app/model/carrinho/carrinho.component';
import { UsuarioService } from 'src/app/service/usuario.service';
import { UsuarioModel } from 'src/app/model/usuario/usuario.component';

@Component({
  selector: 'app-dialog-alerta',
  templateUrl: './dialog-alerta.component.html',
  styleUrls: ['./dialog-alerta.component.css']
})
export class DialogAlertaComponent implements OnInit {
  message: string;
  id: number;
  produto: ProdutoModel[];
  carrinho: CarrinhoModel[];
  usuario:UsuarioModel[];
  action: boolean;
  delete: string;

  constructor(private usuarioService: UsuarioService,private carrinhoService: CarrinhoService, private messageService: MessageService, private produtoService: ProdutoService, public dialogRef: MatDialogRef<DialogAlertaComponent>) { }

  ngOnInit() {
    //this.id= this.produtoService.id;
    this.produto = this.produtoService.produto;
    this.carrinho = this.carrinhoService.carrinho;
    this.usuario = this.usuarioService.usuario;
    this.message = this.messageService.message;
    this.action = this.messageService.action;
    this.delete = this.messageService.delete

    console.log(this.delete)
  }

  onCloseDialog(): void {
    this.dialogRef.close();

  }

  onDelete() {
    if (this.delete == "produto") {
      this.deleteProduto();

    } else if (this.delete == "carrinho") {
      this.deleteCarrinho();

    } else if (this.delete == "usuario") {
      this.deleteUsuario();
    }

  }

  deleteProduto() {
    this.produto.forEach(item => {
      this.produtoService.delete(item.id).subscribe(response => {
        console.log(response);
      },
        err => {
          alert("Erro na exclusão do registro !")
        }
      );
    })
    this.dialogRef.close();
  }

  deleteCarrinho() {
    this.carrinho.forEach(item => {
      this.carrinhoService.delete(item.id).subscribe(response => {
        console.log(response);
       },
        err => {
          alert("Erro na exclusão do registro !")
        }
      );
    })
    this.dialogRef.close();
  }

  deleteUsuario(){
    this.usuario.forEach(item => {
      this.usuarioService.delete(item.id).subscribe(response => {
        console.log(response);
        this.dialogRef.close();
      },
        err => {
          alert("Erro na exclusão do registro !")
        }
      );
    })
    
  }
}



