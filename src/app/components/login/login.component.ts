import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UsuarioService } from 'src/app/service/usuario.service';
import { UsuarioModel } from 'src/app/model/usuario/usuario.component';
import { element } from 'protractor';
import { HomeComponent } from '../home/home.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  usuario: UsuarioModel[] = [];
  
  constructor(private router: Router, private usuarioService: UsuarioService) { }
  email: string;
  password: string;

  ngOnInit() {
  }

  loginDb() {
    this.usuarioService.login(this.email).subscribe(response => {
      this.usuario = response as UsuarioModel[];
      if (this.usuario.length == 0) {
        alert("Dados inexistente");
      } else {
        this.usuario.forEach(item => {
          //if (item.email == this.email && item.senha == this.password) {
          if (this.email == "admin" && this.password == "admin") {
            this.router.navigate(['/home']);
            this.usuarioService.usuarioDto.matricula = item.matricula;
          } else {
            alert("Email ou senha inválidos");
          }
        });
      }
    });
  }

  login() {
    this.usuario = this.usuarioService.getAllMock();
    this.usuario.forEach( item => {
      this.router.navigate(['/home']);
      this.usuarioService.usuarioDto.matricula = item.matricula;

    })
    //if (this.email == "admin" && this.password == "admin") {
  
    // } else {
    // alert("Email ou senha inválidos");
    // }

  }
}



