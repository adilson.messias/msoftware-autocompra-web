import { Component, OnInit } from '@angular/core';
import { SaleItensModel } from 'src/app/model/sale-Itens/sale.itens.component';
import { MatTableDataSource, MatDialog } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
import { SaleItensService } from 'src/app/service/sale-itens.service';
import { Router } from '@angular/router';
import { TableUtil } from '../produto/tableUtils';
import { stringify } from 'querystring';
import { SalesModel } from 'src/app/model/sales/sales.component';
import { MessageService } from 'src/app/service/message.service';

@Component({
  selector: 'app-sale-itens',
  templateUrl: './sale-itens.component.html',
  styleUrls: ['./sale-itens.component.css']
})
export class SaleItensComponent implements OnInit {
  transactions = new MatTableDataSource<SaleItensModel>(null);
  displayedColumns: string[] = ['codBarrasItem', 'descItem', 'valorUnit', 'qtdItem', 'valorTotal',];
  selection = new SelectionModel<SaleItensModel>(true, []);
  dataSource: SalesModel[] = [];
  dataVenda: Date;
  cliente: number;
  idItens: number;

  constructor(private messageService: MessageService, private salesItensService: SaleItensService, private router: Router, private saleItensService: SaleItensService, private dialog: MatDialog) { }

  ngOnInit() {
    this.dataSource = this.saleItensService.salesDto;
    this.getHeader();
    this.getSalesItens();
    this.getTotal();
  }

  getHeader() {
    this.dataSource.forEach(item => {
      this.dataVenda = item.data;
      this.cliente = item.idCliente;
      this.idItens = item.idItens
    });

  }

  getSalesItens() {
    this.salesItensService.getItensByCliente(this.messageService.id, this.messageService.idItens).subscribe(response => {
      this.transactions.data = response as SaleItensModel[];
    });
  }

  getTotal() {
    return this.transactions.data.map(t => t.valorTotal).reduce((acc, value) => acc + value, 0);
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.transactions.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.transactions.data.forEach(row => this.selection.select(row));

  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: SaleItensModel): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.id + 1}`;

  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.transactions.filter = filterValue;
  }

  onSaleDatail() {
    console.log(this.selection.selected)
  }

  onReturn() {
    this.router.navigate(['/sales']);
  }

  exportTable() {
    var name: string;
    name = "Itens da Venda"
    TableUtil.exportToExcel("ProdutoTable", name);
  }
}
