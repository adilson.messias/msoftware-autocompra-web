import { Component, OnInit } from '@angular/core';
import { CarrinhoService } from 'src/app/service/carrinho.service';
import { CarrinhoModel } from 'src/app/model/carrinho/carrinho.component';
import { MatTableDataSource } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
import { SaleItensModel } from 'src/app/model/sale-Itens/sale.itens.component';
import { SaleItensService } from 'src/app/service/sale-itens.service';
import { MonitorService } from 'src/app/service/monitor.service';
import { CompraModel } from 'src/app/model/carrinho/compra.component';


@Component({
  selector: 'app-sale-monitor',
  templateUrl: './sale-monitor.component.html',
  styleUrls: ['./sale-monitor.component.css']
})
export class SaleMonitorComponent implements OnInit {
  transactions = new MatTableDataSource<CompraModel>(null);
  dataSource = new MatTableDataSource<CarrinhoModel>(null);
  displayedColumns: string[] = ['select','id', 'codBarras','status',];
  displayedColumnsMonitor: string[] = ['codBarrasItem', 'descItem', 'valorUnit', 'qtdItem', 'valorTotal','pesoItens','pesoCarrinho'];
  selection = new SelectionModel<CarrinhoModel>(true, []);
  


  constructor(private monitorService : MonitorService ,private saleItensService: SaleItensService ,private carrinhoService: CarrinhoService) { }

  ngOnInit() {
   this.findCarrinhoOperante()

  }

  findCarrinhoOperante(){
    this.monitorService.getOperanteAll().subscribe(response =>{
     this.dataSource.data = response as CarrinhoModel[];
    });
  }

   /** Whether the number of selected elements matches the total number of rows. */
   isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));

  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: CarrinhoModel): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.id + 1}`;

  }
  onCarrinhoDetail(){
    this.selection.selected.forEach(item =>{
      let tableName = item.codBarras;
      this.monitorService.getByItem(tableName).subscribe(response =>{
         this.transactions.data = response as CompraModel[];
         this.transactions.data.forEach(element =>{
         element.pesoCarrinho =  element.pesoProd * element.qtdProd;
        })
       
    })
  });

  }

  getTotal() {
    return this.transactions.data.map(t => t.valorTotal).reduce((acc, value) => acc + value, 0);
  }

  getTotalPesoItem(){
    //return this.transactions.data.map(t => t.pesoItens).reduce((acc, value) => acc + value, 0);
  }

  getTotalPesoCarrinho(){
    return this.transactions.data.map(t => t.pesoCarrinho).reduce((acc, value) => acc + value, 0);
  }



}
