import { Component, OnInit } from '@angular/core';
import { Route } from '@angular/compiler/src/core';
import { Router } from '@angular/router';
import { UsuarioService } from 'src/app/service/usuario.service';
import { UsuarioModel } from 'src/app/model/usuario/usuario.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  usuario = new UsuarioModel();

  constructor(private router: Router, private usuarioService: UsuarioService) { }

  ngOnInit() {
    this.usuario.matricula = this.usuarioService.usuarioDto.matricula;

  }

  open(menu) {
    menu.openMenu();
  }

  onPageNavigation(value) {
    if (value == 1) {
      this.router.navigate(["/produto"]);
      value = null;
    } else if (value == 2) {
      this.router.navigate(["/consulta"]);
      value = null;
    } else if (value == 3) {
      this.router.navigate(["/sales"]);
    } else if (value == 4) {
      this.router.navigate(["/sale-monitor"]);
    } else if (value == 5) {
      this.router.navigate(["/carrinho"]);
    } else if (value == 6) {
      this.router.navigate(["/compra"]);
     } else {
      this.router.navigate(["/login"]);
    }

  }
}
