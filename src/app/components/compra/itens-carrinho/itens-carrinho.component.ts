import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
import { CompraService } from 'src/app/service/compra.service';
import { CompraModel } from 'src/app/model/carrinho/compra.component';
import { MessageService } from 'src/app/service/message.service';
import { Router } from '@angular/router';
import { SalesModel } from 'src/app/model/sales/sales.component';
import { SalesService } from 'src/app/service/sales.service';
import { CarrinhoModel } from 'src/app/model/carrinho/carrinho.component';
import { SaleItensService } from 'src/app/service/sale-itens.service';
import { SaleItensModel } from 'src/app/model/sale-Itens/sale.itens.component';
import { PagSeguroItemModel } from 'src/app/model/pag-seguro/pagseguro.item.component';
import { PagseguroService } from 'src/app/service/pagseguro.service';

@Component({
  selector: 'app-itens-carrinho',
  templateUrl: './itens-carrinho.component.html',
  styleUrls: ['./itens-carrinho.component.css']
})
export class ItensCarrinhoComponent {
  //transactions = new MatTableDataSource<CompraModel>(null);
  dataSource = new MatTableDataSource<CompraModel>(null);
  displayedColumns: string[] = ['codBarrasProd', 'descProd', 'precoUnit', 'qtdProd', 'valorTotal', 'delete'];
  selection = new SelectionModel<CompraModel>(true, []);
  carrinhoUpdate = new CarrinhoModel();
  carrinho: number;
  cliente: number;
  data: Date = new Date();
  pagSeguroItem: PagSeguroItemModel;
  listPagSeguroItem: PagSeguroItemModel[] = [];
  urlPagSeguro: any

  constructor(private pagseguroService: PagseguroService, private saleItemService: SaleItensService, private saleService: SalesService, private router: Router, private compraService: CompraService, private messageService: MessageService) { }

  ngOnInit() {
    this.carrinho = this.messageService.tableName;
    this.getAll();
  }

  getAll() {
    let table = this.carrinho
    this.compraService.getAll(table).subscribe(response => {
      this.dataSource.data = response as CompraModel[];
      this.dataSource.data.forEach(element => {
        this.carrinho = element.tableName;
        this.cliente = element.codCliente;
        this.data = element.data;

      })
    },
      err => {
        alert("Ocorreu um erro de conexão com o servidor. Verifique e tente novamente !")
        this.messageService.action = false;
      }
    );
  }

  getTotal() {
    return this.dataSource.data.map(t => t.valorTotal).reduce((acc, value) => acc + value, 0);
  }

  getTotalItem() {
    return this.dataSource.data.map(t => t.qtdProd).reduce((acc, value) => acc + value, 0);
  }


  excluirItem(item) {
    this.compraService.deleteItem(item).subscribe(response => {
      this.getAll();
    },
      err => {
        alert("Erro na exclusão do registro !")
      });

  }

  onReturn() {
    this.messageService.tableName = this.carrinho
    this.router.navigate(['/compra']);
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));

  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: CompraModel): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.id + 1}`;

  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  onPagamentoOFF() {
    this.dataSource.data.forEach(item => {
      let pagSeguroItem = new PagSeguroItemModel();
      pagSeguroItem.codBarrasItem = item.codBarrasProd.toString();
      pagSeguroItem.descProd = item.descProd;
      pagSeguroItem.qtdItens = item.qtdProd;
      pagSeguroItem.valorItem = item.valorTotal;
      this.listPagSeguroItem.push(pagSeguroItem);
    })

    this.pagseguroService.pagSeguro("messias", "messi@email", "11", "97675656", this.listPagSeguroItem).subscribe(response => {
      this.urlPagSeguro = response.trim();
    },
      err => {
        alert("erro de autenticaçao")
        this.messageService.action = false;
      }

    );

  }

  onPagamento() {
    // checkout pagSeguro
    // salvar venda
    //gerar codigo de saida
    //salvar codigo de saide
    // update status carrinho

    this.updateStatusCarrinho();
    let sale = new SalesModel;
    sale.data = this.data;
    sale.id = 0;
    sale.tableName = this.carrinho;
    sale.idCliente = this.cliente;
    sale.idItens = this.cliente;
    sale.valorTotal = this.getTotal();
    this.saleService.save(sale).subscribe(response => {
      this.insertSalesItemCarrinho();
    },
      err => {
        alert("Ocorreu um erro de conexão com o servidor. Verifique e tente novamente !")
        this.messageService.action = false;
      }

    );

  }

  updateStatusCarrinho() {
    this.carrinhoUpdate.codBarras = this.carrinho;
    this.carrinhoUpdate.status = "Inoperante"
    this.compraService.update(this.carrinhoUpdate).subscribe(item => {
    });
  }

  insertSalesItemCarrinho() {
    this.dataSource.data.forEach(item => {
      let saleItensModel = new SaleItensModel();
      saleItensModel.id = item.id;
      saleItensModel.idCliente = this.cliente;
      saleItensModel.idItens = this.cliente;
      saleItensModel.codBarrasProd = item.codBarrasProd
      saleItensModel.descProd = item.descProd;
      saleItensModel.precoUnit = item.precoUnit;
      saleItensModel.qtdItens = item.qtdProd;
      saleItensModel.valorTotal = item.valorTotal;
      this.saleItemService.save(saleItensModel, this.carrinho).subscribe(response => {


      })

    })


  }
}
