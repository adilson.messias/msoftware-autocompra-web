import { Component, OnInit } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { MatDialog } from '@angular/material';
import { FormatsDialogComponent } from './formats-dialog/formats-dialog.component';
import { AppInfoDialogComponent } from './app-info-dialog/app-info-dialog.component';
import { BarcodeFormat } from '@zxing/library';
import { ProdutoService } from 'src/app/service/produto.service';
import { ProdutoComponent } from '../produto/produto.component';
import { ProdutoEstoqueModel } from 'src/app/model/produto/produto.estoque.component';
import { element } from 'protractor';
import { CompraService } from 'src/app/service/compra.service';
import { Router } from '@angular/router';
import { CompraModel } from 'src/app/model/carrinho/compra.component';
import { MessageService } from 'src/app/service/message.service';
import { DialogAlertaComponent } from '../dialog-alerta/dialog-alerta.component';
import { CarrinhoService } from 'src/app/service/carrinho.service';
import { CarrinhoModel } from 'src/app/model/carrinho/carrinho.component';

@Component({
  selector: 'app-compra',
  templateUrl: './compra.component.html',
  styleUrls: ['./compra.component.css']
})

export class CompraComponent {
  item: ProdutoEstoqueModel[] = [];
  availableDevices: MediaDeviceInfo[];
  carrinho = new CarrinhoModel();
  currentDevice: MediaDeviceInfo = null;
  codeBarra: number;
  pesoProd: number;
  descProd: string;
  precoUnit: number;
  code: number;
  qtd: number;
  total: number;
  data: Date = new Date();
  msg: string;
  tableName: number;
  action: string;
  peso: number;
  status: string;
  idCliente: number;

  formatsEnabled: BarcodeFormat[] = [
    BarcodeFormat.CODE_128,
    BarcodeFormat.DATA_MATRIX,
    BarcodeFormat.EAN_13,
    BarcodeFormat.QR_CODE,
  ];

  hasDevices: boolean;
  hasPermission: boolean;

  qrResultString: number;
  qrResultStringButtom: number;

  torchEnabled = false;
  torchAvailable$ = new BehaviorSubject<boolean>(false);
  tryHarder = false;

  ngOnInit() {
    this.msg = this.messageService.message;
    this.action = this.msg;
    this.tableName = this.messageService.tableName;
    this.idCliente = this.messageService.id;
    console.log(this.action);

  }

  constructor(private carrinhoService: CarrinhoService, private dialog: MatDialog, private messageService: MessageService, private router: Router, private compraService: CompraService, private produtoService: ProdutoService, private readonly _dialog: MatDialog) { }

  clearResult(): void {
    this.qrResultString = null;
    this.qrResultStringButtom = null;
    this.code = null;
  }

  onCamerasFound(devices: MediaDeviceInfo[]): void {
    this.availableDevices = devices;
    this.hasDevices = Boolean(devices && devices.length);
  }

  onCodeResult(resultString: number) {
    if (this.msg  == "scan_carrinho") {
      this.messageService.action = true;
      this.msg = "Boas Compras !"
      this.message();
      this.msg = "";
    } else {
      this.qrResultString = resultString;
      //this.qrResultStringButtom = resultString;
      console.log(resultString);
      this.findProdutoByBarCode(resultString);
    }
  }

  message() {
    this.messageService.message = this.msg;
    this.openDialogAlerta();
  }

  openDialogAlerta() {
    const dialogRef = this.dialog.open(DialogAlertaComponent, { width: '300px', });
    dialogRef.afterClosed().subscribe(result => {

    });
  }

  findProdutoByBarCode(code) {
    this.codeBarra = null;
    this.pesoProd = null;
    this.descProd = null;
    this.precoUnit = null;
    this.produtoService.findProdutoByBarCode(code).subscribe(response => {
      this.item = response as ProdutoEstoqueModel[];
      this.item.forEach(element => {
        this.codeBarra = element.codBarrasProd;
        this.pesoProd = element.pesoProd;
        this.descProd = element.descProd;
        this.precoUnit = element.precoUnit;
        this.qtd = 1;
        this.total = this.precoUnit * this.qtd;
      })
    },
      err => {
        alert("Ocorreu um erro de conexão com o servidor. Verifique e tente novamente !")
        this.messageService.action = false;
      }
    );
  }


  onDeviceSelectChange(selected: string) {
    const device = this.availableDevices.find(x => x.deviceId === selected);
    this.currentDevice = device || null;
  }

  openFormatsDialog() {
    const data = {
      formatsEnabled: this.formatsEnabled,
    };

    this._dialog
      .open(FormatsDialogComponent, { data })
      .afterClosed()
      .subscribe(x => { if (x) { this.formatsEnabled = x; } });
  }

  onHasPermission(has: boolean) {
    this.hasPermission = has;
  }

  openInfoDialog() {
    const data = {
      hasDevices: this.hasDevices,
      hasPermission: this.hasPermission,
    };

    this._dialog.open(AppInfoDialogComponent, { data });
  }

  onTorchCompatible(isCompatible: boolean): void {
    this.torchAvailable$.next(isCompatible || false);
  }

  toggleTorch(): void {
    this.torchEnabled = !this.torchEnabled;
  }

  toggleTryHarder(): void {
    this.tryHarder = !this.tryHarder;
  }

  onAddItemCarrinho() {
    let carrinho = new CompraModel()
    this.item.forEach(element => {
      carrinho.codBarrasProd = element.codBarrasProd;
      carrinho.descProd = element.descProd
      carrinho.id = element.id;
      carrinho.precoUnit = element.precoUnit;
      carrinho.pesoProd = element.pesoProd
      carrinho.qtdProd = this.qtd;
      carrinho.valorTotal = this.total;
      carrinho.tableName = this.tableName;
      carrinho.codCliente = this.idCliente;
      carrinho.data = this.data;
      console.log(carrinho);
    });
    this.compraService.addItemCarrinho(carrinho).subscribe(response => {
    },
      err => {
        alert("Erro na inclusão do registro !")
      }

    );

    this.clearResult();
    this.code = null;

  }

  findByItemBarCode() {
    if (this.msg == "scan_carrinho") {
      if (this.code == null) {
        this.messageService.action = true;
        this.msg = "Para iniciar as compras, escaneie ou digite o código de barras do carrinho."
        this.message();
      } else {
        this.tableName = this.code;
        this.messageService.tableName = this.tableName;
        this.carrinho.codBarras = this.tableName;
        this.carrinho.status = "Em Operação";
        this.compraService.update(this.carrinho).subscribe(item => {

        });
        this.messageService.action = true;
        this.msg = "Agora é so escanear o código de barras dos produtos e adicionar no carrinho.Boas Compras!"
        this.message();
        this.action = null;
        this.code = null;

      }
    } else {
      this.qrResultString = this.code;
      this.findProdutoByBarCode(this.code);
    }
  }

  onViewCar() {
    this.router.navigate(['/itens-carrinho'])

  }
  qtdUp() {
    this.qtd = this.qtd + 1;
    this.total = this.precoUnit * this.qtd;
    this.peso = this.pesoProd * this.qtd;

  }

  qtdDown() {
    if (this.qtd == 1) {
      this.qtd = 1;
    } else {
      this.qtd = this.qtd - 1;
      this.total = this.precoUnit * this.qtd;
      this.peso = this.pesoProd * this.qtd;

    }
  }
}
