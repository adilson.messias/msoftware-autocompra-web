import { Component, OnInit } from '@angular/core';
import { UsuarioModel } from 'src/app/model/usuario/usuario.component';
import { UsuarioService } from 'src/app/service/usuario.service';
import { MatDialogRef } from '@angular/material';

export interface Acesso {
  value: string;
  //viewValue: string;
}


@Component({
  selector: 'app-dialog-cadastro-user',
  templateUrl: './dialog-cadastro-user.component.html',
  styleUrls: ['./dialog-cadastro-user.component.css']
})
export class DialogCadastroUserComponent implements OnInit {

  constructor(private usuarioService: UsuarioService, public dialogRef: MatDialogRef<DialogCadastroUserComponent>) { }
  usuario = new UsuarioModel();
  usuarioModel: UsuarioModel[];
  message: string;

  ngOnInit() {
    this.usuario.id = this.usuarioService.usuarioDto.id;
    this.usuario.email = this.usuarioService.usuarioDto.email;
    this.usuario.matricula = this.usuarioService.usuarioDto.matricula;
    this.usuario.senha = this.usuarioService.usuarioDto.senha;
    this.usuario.perfil = this.usuarioService.usuarioDto.perfil;

  }

  onCloseDialog(): void {
    this.dialogRef.close();

  }

  onSave() {
    console.log(this.usuario)
    this.usuarioService.save(this.usuario).subscribe(response => {
    });
    this.dialogRef.close();
  }

  onValidateForm() {
    if (this.usuario.email == null ||
      this.usuario.matricula == null ||
      this.usuario.senha == null ||
      this.usuario.perfil == null) {
      this.message = "Preencha todos os campos obrigatórios!"
    } else {
      this.onSave();
    }
  }

  cleanForm() {
    this.usuario.id = null;
    this.usuario.email = null;
    this.usuario.matricula = null;
    this.usuario.senha = null;
    this.usuario.perfil = null;

  }

  perfis: Acesso[] = [
    { value: 'somente leitura' },
    { value: 'leitura e gravação' },
    { value: 'leitura, gravação e alteração' }
  ];

  changeClient(value) {
    this.usuario.perfil = value;
  }

}
