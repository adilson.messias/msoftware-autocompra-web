import { Component, OnInit } from '@angular/core';
import { UsuarioModel } from 'src/app/model/usuario/usuario.component';
import { UsuarioService } from 'src/app/service/usuario.service';
import { MatDialog, MatTableDataSource } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
import { TableUtil } from '../produto/tableUtils';
import { DialogCadastroUserComponent } from './cadastro/dialog-cadastro-user/dialog-cadastro-user.component';
import { DialogAlertaComponent } from '../dialog-alerta/dialog-alerta.component';
import { MessageService } from 'src/app/service/message.service';


@Component({
  selector: 'app-consulta',
  templateUrl: './consulta.component.html',
  styleUrls: ['./consulta.component.css']
})
export class ConsultaComponent implements OnInit {
  id: number;
  msg: string;
  dataSource = new MatTableDataSource<UsuarioModel>(null);
  displayedColumns: string[] = ['select', 'id','matricula', 'perfil','senha','email'];
  selection = new SelectionModel<UsuarioModel>(true, []);

  constructor(private messageService: MessageService, private usuarioService: UsuarioService, private dialog: MatDialog) { }
  ngOnInit() {
    this.getAll();
  }

  getAll() {
    this.usuarioService.getAll().subscribe(response => {
      this.dataSource.data = response as UsuarioModel[];
    },
      err => {
        alert("Ocorreu um erro de conexão com o servidor. Verifique e tente novamente !")
        this.messageService.action = false;
      }
    );
   }

  exportTable() {
    TableUtil.exportToExcel("UsuarioTable");
  }

  message() {
    this.messageService.delete="usuario"
    this.messageService.message = this.msg;
    this.openDialogAlerta();
  }

  editarItem() {
    this.id = null;
    this.selection.selected.forEach(element => {
      this.addFields(element)
      this.id = element.id;
    });
    if (this.id == null) {
      this.msg = "Selecione um registro para editar !"
      this.message();
      this.messageService.action = false;
    } else {
      this.openDialog();
    }
  }

  addNewItem() {
    this.cleanFields();
  }

  addFields(element) {
    this.usuarioService.usuarioDto.id = element.id;
    this.usuarioService.usuarioDto.email = element.email;
    this.usuarioService.usuarioDto.senha = element.senha;
    this.usuarioService.usuarioDto.matricula = element.matricula;
    this.usuarioService.usuarioDto.perfil = element.perfil;


  }

  cleanFields() {
    this.usuarioService.usuarioDto.id = null;
    this.usuarioService.usuarioDto.email = null;
    this.usuarioService.usuarioDto.senha = null;
    this.usuarioService.usuarioDto.matricula = null;
    this.usuarioService.usuarioDto.perfil = null;

  }

  excluirItem() {
    console.log(this.selection.selected)
    this.id = null;
    this.selection.selected.forEach(element => {
      this.id = element.id;
      this.usuarioService.usuario = this.selection.selected;
    });
    if (this.id == null) {
      this.msg = "Selecione um registro para excluir !"
      this.message();
      this.messageService.action = false;
    } else {
      this.messageService.action = true;
      this.msg = "Deseja realmente excluir o registro ?"
      this.message();

    }

  }

  onAction(action) {
    if (action == 0) {
      this.addNewItem();
      this.openDialog();
    } else {
      this.editarItem();
    }
  }

  openDialog() {
    const dialogRef = this.dialog.open(DialogCadastroUserComponent, { width: '300px', });
    dialogRef.afterClosed().subscribe(result => {
      this.getAll();
      this.selection.clear();
      this.cleanFields();
    });

  }

  openDialogAlerta() {
    const dialogRef = this.dialog.open(DialogAlertaComponent, { width: '300px', });
    dialogRef.afterClosed().subscribe(result => {
      this.getAll();
      this.selection.clear();
    });

  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));

  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: UsuarioModel): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.id + 1}`;

  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

}

