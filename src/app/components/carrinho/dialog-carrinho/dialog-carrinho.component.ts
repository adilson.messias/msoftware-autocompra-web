import { Component, OnInit } from '@angular/core';
import { CarrinhoModel } from 'src/app/model/carrinho/carrinho.component';
import { CarrinhoService } from 'src/app/service/carrinho.service';
import { MatDialogRef, throwMatDialogContentAlreadyAttachedError } from '@angular/material';

@Component({
  selector: 'app-dialog-carrinho',
  templateUrl: './dialog-carrinho.component.html',
  styleUrls: ['./dialog-carrinho.component.css']
})
export class DialogCarrinhoComponent implements OnInit {

 
  constructor(private carrinhoService: CarrinhoService, public dialogRef: MatDialogRef<DialogCarrinhoComponent>) { }
  carrinho = new CarrinhoModel();
  carrinhoModel: CarrinhoModel[];
  hide:boolean = false;
  tableName: number;


  ngOnInit() {
    this.carrinho.codBarras = this.carrinhoService.carrinhoDto.codBarras;
    this.carrinho.status = this.carrinhoService.carrinhoDto.status;
    this.carrinho.id = this.carrinhoService.carrinhoDto.id;
   
  }

  onCloseDialog(): void {
    this.dialogRef.close();

  }

  onSaveAll(){
    this.onSave();
    this.saveTable();
   
   
  }

  onSave() {
    this.carrinho.status="inoperante"
    this.carrinhoService.save(this.carrinho).subscribe(response => {
     },
      err => {
        alert("Erro na operação executada !")
      }

    );
    
  }

  saveTable() {
  this.tableName = this.carrinho.codBarras;
   this.carrinhoService.saveTable(this.tableName).subscribe(response => {
    this.dialogRef.close();
    },
      err => {
        alert("Erro na operação executada !")
      }

    );
   
  }

  onValidateForm() {
    if (this.carrinho.codBarras == null ){
        alert("Preencha todos os campos obrigatórios !");
    } else {
      this.onSaveAll();
    }
  }

  cleanForm() {
    this.carrinho.codBarras = null;
    this.carrinho.status = null
    
  

  }
}
