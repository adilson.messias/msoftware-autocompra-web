import { Component, OnInit } from '@angular/core';
import { CarrinhoModel } from 'src/app/model/carrinho/carrinho.component';
import { MatTableDataSource, MatDialog } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
import { MessageService } from 'src/app/service/message.service';
import { DialogComponent } from '../produto/dialog/dialog.component';
import { DialogAlertaComponent } from '../dialog-alerta/dialog-alerta.component';
import { CarrinhoService } from 'src/app/service/carrinho.service';
import { TableUtil } from '../produto/tableUtils';
import { DialogCarrinhoComponent } from './dialog-carrinho/dialog-carrinho.component';

@Component({
  selector: 'app-carrinho',
  templateUrl: './carrinho.component.html',
  styleUrls: ['./carrinho.component.css']
})
export class CarrinhoComponent implements OnInit {
  msg: string;
  id: number;
  action: boolean = false;
  dataSource = new MatTableDataSource<CarrinhoModel>(null);
  displayedColumns: string[] = ['select', 'id', 'codBarras', 'status',];
  selection = new SelectionModel<CarrinhoModel>(true, []);
  carrinho:string

  constructor(private messageService: MessageService, private carrinhoService: CarrinhoService, private dialog: MatDialog) { }

  ngOnInit() {
    this.getAll();
  }

  getAll() {
     this.carrinhoService.getAll().subscribe(response => {
      this.dataSource.data = response as CarrinhoModel[];
    },
      err => {
        alert("Ocorreu um erro de conexão com o servidor. Verifique e tente novamente !")
        this.messageService.action = false;
      }
    );
    
  }

  message() {
    this.messageService.delete="carrinho"
    this.messageService.message = this.msg;
    this.openDialogAlerta();
  }

  exportTable() {
    var name: string;
    name = "Carrinhos";
    TableUtil.exportToExcel("ProdutoTable", name);
  }

  editarItem() {
    this.id = null;
    this.selection.selected.forEach(element => {
      this.addFields(element)
      this.id = element.id;
    });
    if (this.id == null) {
      this.msg = "Selecione um registro para editar !"
      this.message();
      this.messageService.action = false;
    } else {
      this.openDialog();
    }
  }

  excluirItem() {
    console.log(this.selection.selected)
    this.id = null;
    this.selection.selected.forEach(element => {
      this.id = element.id;
      this.carrinhoService.carrinho = this.selection.selected;
    });
    if (this.id == null) {
      this.msg = "Selecione um registro para excluir !"
      this.message();
      this.messageService.action = false;
    } else {
      this.messageService.action = true;
      this.msg = "Deseja realmente excluir o registro ?"
      this.message();

    }

  }

  addNewItem() {
    this.cleanFields();
  }

  addFields(element) {
    this.carrinhoService.carrinhoDto.codBarras = element.codBarras;
    this.carrinhoService.carrinhoDto.status = element.status;
    this.carrinhoService.carrinhoDto.id = element.id;

  }

  cleanFields() {
    this.carrinhoService.carrinhoDto.codBarras = null;
    this.carrinhoService.carrinhoDto.status = null;
    this.carrinhoService.carrinhoDto.id = null;

  }

  openDialog() {
    const dialogRef = this.dialog.open(DialogCarrinhoComponent, { width: '300px', });
    dialogRef.afterClosed().subscribe(result => {
      this.selection.clear();
      this.cleanFields();
     this.getAll();
     
    });
    
  }

  openDialogAlerta() {
    const dialogRef = this.dialog.open(DialogAlertaComponent, { width: '300px', });
    dialogRef.afterClosed().subscribe(result => {
      this.getAll();
      this.selection.clear();
    });
  }

  onAction(action) {
    if (action == 0) {
      this.addNewItem();
      this.openDialog();
    } else {
      this.editarItem();
    }
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));

  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: CarrinhoModel): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.id + 1}`;

  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }


}
