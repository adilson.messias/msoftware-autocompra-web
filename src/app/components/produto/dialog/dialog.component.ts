import { Component, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ProdutoModel } from 'src/app/model/produto/produto.component';
import { ProdutoService } from 'src/app/service/produto.service';


@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.css']
})
export class DialogComponent {

  constructor(private produtoService: ProdutoService, public dialogRef: MatDialogRef<DialogComponent>) { }
  produto = new ProdutoModel();
  produtoModel: ProdutoModel[];
  hide:boolean = false;


  ngOnInit() {
    this.produto.codBarrasProd = this.produtoService.produtoDto.codBarrasProd;
    this.produto.descProd = this.produtoService.produtoDto.descProd;
    this.produto.id = this.produtoService.produtoDto.id;
    this.produto.pesoProd = this.produtoService.produtoDto.pesoProd;
    this.produto.precoUnit = this.produtoService.produtoDto.precoUnit;

  }

  onCloseDialog(): void {
    this.dialogRef.close();

  }

  onSave() {
    this.produtoService.save(this.produto).subscribe(response => {
      this.dialogRef.close();
    },
      err => {
        alert("Erro na operação executada !")
      }

    );
   
  }

  onValidateForm() {
    if (this.produto.codBarrasProd == null ||
      this.produto.descProd == null ||
      this.produto.pesoProd == null ||
      this.produto.precoUnit == null) {
      alert("Preencha todos os campos obrigatórios !")
    } else {
      this.onSave();
    }
  }

  cleanForm() {
    this.produto.codBarrasProd = null;
    this.produto.descProd = null
    this.produto.pesoProd = null;
    this.produto.precoUnit = null;
    this.produto.id = null;

  }


}

