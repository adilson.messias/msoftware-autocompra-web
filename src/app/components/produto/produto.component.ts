import { Component, OnInit } from '@angular/core';
import { TableUtil } from './tableUtils';
import { ProdutoService } from 'src/app/service/produto.service';
import { ProdutoModel } from 'src/app/model/produto/produto.component';
import { DialogComponent } from './dialog/dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { SelectionModel } from '@angular/cdk/collections';
import { element } from 'protractor';
import { DialogAlertaComponent } from '../dialog-alerta/dialog-alerta.component';
import { MatTableDataSource } from '@angular/material';
import { MessageService } from 'src/app/service/message.service';

@Component({
  selector: 'app-produto',
  templateUrl: './produto.component.html',
  styleUrls: ['./produto.component.css']
})

export class ProdutoComponent implements OnInit {
  //produto: ProdutoModel[] = []
  msg: string;
  id: number;
  action: boolean = false;
  dataSource = new MatTableDataSource<ProdutoModel>(null);
  displayedColumns: string[] = ['select', 'id', 'codBarrasProd', 'descProd', 'pesoProd', 'precoUnit',];
  selection = new SelectionModel<ProdutoModel>(true, []);

  constructor(private messageService: MessageService, private produtoService: ProdutoService, private dialog: MatDialog) { }


  ngOnInit() {
    this.getAll();
  }

  getAll() {
    this.produtoService.getAll().subscribe(response => {
      this.dataSource.data = response as ProdutoModel[];
    },
      err => {
        alert("Ocorreu um erro de conexão com o servidor. Verifique e tente novamente !")
        this.messageService.action = false;
      }
    );
  }

  message() {
    this.messageService.delete="produto"
    this.messageService.message = this.msg;
    this.openDialogAlerta();
  }

  exportTable() {
    var name: string;
    name = "Produtos";
    TableUtil.exportToExcel("ProdutoTable", name);
  }

  editarItem() {
    this.id = null;
    this.selection.selected.forEach(element => {
      this.addFields(element)
      this.id = element.id;
    });
    if (this.id == null) {
      this.msg = "Selecione um registro para editar !"
      this.message();
      this.messageService.action = false;
    } else {
      this.openDialog();
    }
  }

  addNewItem() {
    this.cleanFields();
  }

  addFields(element) {
    this.produtoService.produtoDto.codBarrasProd = element.codBarrasProd;
    this.produtoService.produtoDto.descProd = element.descProd;
    this.produtoService.produtoDto.id = element.id;
    this.produtoService.produtoDto.pesoProd = element.pesoProd;
    this.produtoService.produtoDto.precoUnit = element.precoUnit;
  }

  cleanFields() {
    this.produtoService.produtoDto.codBarrasProd = null;
    this.produtoService.produtoDto.descProd = null;
    this.produtoService.produtoDto.id = null;
    this.produtoService.produtoDto.pesoProd = null;
    this.produtoService.produtoDto.precoUnit = null;
  }

  excluirItem() {
    console.log(this.selection.selected)
    this.id = null;
    this.selection.selected.forEach(element => {
      this.id = element.id;
      this.produtoService.produto = this.selection.selected;
    });
    if (this.id == null) {
      this.msg = "Selecione um registro para excluir !"
      this.message();
      this.messageService.action = false;
    } else {
      this.messageService.action = true;
      this.msg = "Deseja realmente excluir o registro ?"
      this.message();

    }

  }

  onAction(action) {
    if (action == 0) {
      this.addNewItem();
      this.openDialog();
    } else {
      this.editarItem();
    }
  }

  openDialog() {
    const dialogRef = this.dialog.open(DialogComponent, { width: '300px', });
    dialogRef.afterClosed().subscribe(result => {
      this.getAll();
      this.selection.clear();
      this.cleanFields();
    });

  }

  openDialogAlerta() {
    const dialogRef = this.dialog.open(DialogAlertaComponent, { width: '300px', });
    dialogRef.afterClosed().subscribe(result => {
      this.getAll();
      this.selection.clear();
    });

  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
      console.log(this.selection.selected)
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: ProdutoModel): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.id + 1}`;

  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }


}



