import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ClienteService } from 'src/app/service/cliente.service';
import { ClienteModel } from 'src/app/model/cliente/cliente.component';
import { MessageService } from 'src/app/service/message.service';
import { AuthService, SocialUser, GoogleLoginProvider, FacebookLoginProvider, } from "angularx-social-login";

@Component({
  selector: 'app-cliente',
  templateUrl: './cliente.component.html',
  styleUrls: ['./cliente.component.css']
})
export class ClienteComponent implements OnInit {
  clienteModel: ClienteModel[] = [];
  email: string;
  password: string;
  private user: SocialUser;
  public authorized: boolean = false;
  provider: string;
  usuario = new ClienteModel();
  emailPesquisa: string;

  constructor(private socialAuthService: AuthService, private messageService: MessageService, private router: Router, private clienteService: ClienteService) { }

  ngOnInit() {

  }

  login() {
    if (this.email == undefined || this.password == undefined) {
      alert("Os campos email e senha são obrigatórios")
    } else {
      this.provider = "padrao";
      this.clienteService.verifyClientebyEmail(this.email, this.provider).subscribe(response => {
        this.clienteModel = response as ClienteModel[];
        if (this.clienteModel.length === 0) {
          this.router.navigate(['/cadastro-cliente']);
        } else {
          this.clienteModel.forEach(item => {
            if (item.emailPadrao == this.email && item.senhaPadrao == this.password) {
              this.emailPesquisa = item.emailPadrao;
              this.usuario.firstName = item.firstName;
              this.usuario.lastName = item.lastName;
              this.findIdClienteByEmail();
            } else {
              alert("Email ou senha inválidos");
            }
          });
        }
      });
    }
  }

  public onSocialLogin(socialPlatform: string) {
    let socialPlatformProvider;
    if (socialPlatform == "facebook") {
      socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;
    } else if (socialPlatform == "google") {
      socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
    }
    this.provider = socialPlatform;
    this.socialAuthService.signIn(socialPlatformProvider).then(
      (userData) => {
        if (userData != null) {
          this.authorized = true;
          this.user = userData;
          this.usuario.name = this.user.name
          this.usuario.emailGoogle = this.user.email
          this.usuario.photoUrlGoogle = this.user.photoUrl
          this.usuario.emailFacebook = this.user.email
          this.usuario.photoUrlFacebook = this.user.photoUrl
          this.usuario.firstName = this.user.firstName
          this.usuario.lastName = this.user.lastName
          this.usuario.authToken = this.user.authToken
          this.usuario.provider = this.user.provider
          this.verifyClienteByEmailSocialLogin();
        }
      }
    );
  }

  public signOut() {
    this.socialAuthService.signOut();
    this.authorized = false;
  }

  verifyClienteByEmailSocialLogin() {
    if (this.usuario.provider.toLowerCase() == "google") {
      this.emailPesquisa = this.usuario.emailGoogle;
      this.provider = this.usuario.provider;
    } else {
      this.emailPesquisa = this.usuario.emailFacebook
      this.provider = this.usuario.provider;
    }
    this.clienteService.verifyClientebyEmail(this.emailPesquisa, this.provider.toLowerCase()).subscribe(response => {
      this.clienteModel = response as ClienteModel[];
      if (this.clienteModel.length === 0) {
        this.clienteService.save(this.usuario).subscribe(response => {
          this.findIdClienteByEmail();
        });

      } else {
        this.clienteModel.forEach(item => {
          this.messageService.id = item.id;
          console.log(item.id)
          this.messageService.message = "scan_carrinho";
          this.router.navigate(['/page-bem-vindo']);
        })
      }
    });
  }

  findIdClienteByEmail() {
    console.log("passou aqui")
    this.clienteService.findIdCliente(this.emailPesquisa, this.provider.toLowerCase(), this.usuario.firstName, this.usuario.lastName).subscribe(response => {
      this.clienteModel = response as ClienteModel[];
      this.clienteModel.forEach(item => {
        this.messageService.id = item.id;
        console.log(item.id)
        this.messageService.message = "scan_carrinho";
        this.router.navigate(['/page-bem-vindo']);
      })
    });
  }
}


