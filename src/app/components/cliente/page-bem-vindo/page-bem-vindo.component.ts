import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MessageService } from 'src/app/service/message.service';

@Component({
  selector: 'app-page-bem-vindo',
  templateUrl: './page-bem-vindo.component.html',
  styleUrls: ['./page-bem-vindo.component.css']
})
export class PageBemVindoComponent implements OnInit {

  constructor(private router: Router, private messageService: MessageService) { }

  ngOnInit() {
  }

  next() {
    this.messageService.message = "scan_carrinho"
    this.router.navigate(['/compra']);

  }

}
