import { Component, OnInit } from '@angular/core';
import { ClienteModel } from 'src/app/model/cliente/cliente.component';
import { ClienteService } from 'src/app/service/cliente.service';
import { MessageService } from 'src/app/service/message.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-cadastro-cliente',
  templateUrl: './cadastro-cliente.component.html',
  styleUrls: ['./cadastro-cliente.component.css']
})
export class CadastroClienteComponent implements OnInit {
  cliente = new ClienteModel();
  clienteModel: ClienteModel;
  clienteResponse: ClienteModel[] = [];
  message: string;
  confirmarSenha: string;

  constructor(private clienteService: ClienteService, private messageService: MessageService, private router: Router) { }

  ngOnInit() {

  }

  onValidateForm() {
    if (this.cliente.emailPadrao != null &&
      this.cliente.firstName != null &&
      this.cliente.lastName != null &&
      this.cliente.senhaPadrao != null &&
      this.confirmarSenha != null &&
      this.cliente.senhaPadrao == this.confirmarSenha) {
      this.onSave();

    } else if (this.cliente.emailPadrao == null ||
      this.cliente.firstName == null ||
      this.cliente.lastName == null ||
      this.cliente.senhaPadrao == null ||
      this.confirmarSenha == null) {
      alert("Preencha todos os campos.")
    } else if (this.cliente.senhaPadrao != this.confirmarSenha) {
      alert("As senhas não coincidem.")
    }

  }

  onSave() {
    this.clienteModel = this.cliente;
    this.clienteModel.name = "";
    this.clienteModel.emailGoogle = "";
    this.clienteModel.photoUrlGoogle = "";
    this.clienteModel.emailFacebook = "";
    this.clienteModel.photoUrlFacebook = "";
    this.clienteModel.authToken = "";
    this.clienteModel.provider = "padrao";
    this.clienteService.save(this.clienteModel).subscribe(response => {
     this.findIdClienteByEmail()
    });

  }
  findIdClienteByEmail() {
    this.clienteService.findIdCliente(this.clienteModel.emailPadrao, "padrao", this.clienteModel.firstName, this.clienteModel.lastName).subscribe(response => {
      this.clienteResponse = response as ClienteModel[];
      this.clienteResponse.forEach(item => {
        this.messageService.id = item.id;
        console.log(item.id)
        this.messageService.message = "scan_carrinho";
        this.router.navigate(['/page-bem-vindo']);
      })
    });
  }
}
