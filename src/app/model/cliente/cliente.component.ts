export class ClienteModel {
  id: number;
  emailPadrao: string;
  senhaPadrao: string;
  name: string;
  emailGoogle: string;
  photoUrlGoogle: string;
  emailFacebook: string;
  photoUrlFacebook: string;
  firstName: string;
  lastName: string;
  authToken: string;
  provider:string;
}
