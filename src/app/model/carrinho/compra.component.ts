export class CompraModel {
  id: number;
  codBarrasProd: number;
  descProd: string;
  pesoProd:number;
  precoUnit: number;
  qtdProd:number;
  valorTotal:number;
  tableName:number;
  codCliente:number;
  data:Date;
  pesoCarrinho:number;
 
}
