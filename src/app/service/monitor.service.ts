import { Injectable } from '@angular/core';
import { CarrinhoModel } from '../model/carrinho/carrinho.component';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import { CompraModel } from '../model/carrinho/compra.component';

@Injectable({
  providedIn: 'root'
})
export class MonitorService {
  
  
  constructor(private http: HttpClient) { }

   getByItem(table): Observable<CompraModel[]> {
    console.log(">>>>",table)
    let httpHeaders = new HttpHeaders();
    httpHeaders.set("Content-Type", 'application/json');
    return this.http.options(`${environment.api_monitor}/monitor?table=`+ table,
      { headers: httpHeaders }).pipe(map(response => <CompraModel[]>response));
  }

  getOperanteAll(): Observable<CarrinhoModel[]> {
    let httpHeaders = new HttpHeaders();
    httpHeaders.set("Content-Type", 'application/json');
    return this.http.get(`${environment.api_monitor}/monitor`,
      { headers: httpHeaders }).pipe(map(response => <CarrinhoModel[]>response));
  }

  
  
}
