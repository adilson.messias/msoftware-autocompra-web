import { Injectable } from '@angular/core';
import { ProdutoModel } from '../model/produto/produto.component';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import { stringify } from 'querystring';

@Injectable({
  providedIn: 'root'
})
export class MessageService {
  message:string;
  action:boolean;
  delete:string;
  tableName:number;
  id:number;
  idItens:number;

}

