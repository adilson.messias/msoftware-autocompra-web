import { Injectable } from '@angular/core';
import { ProdutoModel } from '../model/produto/produto.component';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import { ProdutoEstoqueModel } from '../model/produto/produto.estoque.component';


@Injectable({
  providedIn: 'root'
})
export class ProdutoService {
  produto = new Array<ProdutoModel>();
  produtoDto = new ProdutoModel;

  constructor(private http: HttpClient) { }

  getAllMock(): ProdutoModel[] {
    return <ProdutoModel[]>[
     
    ]
  }

  getAll(): Observable<ProdutoModel[]> {
    let httpHeaders = new HttpHeaders();
    return this.http.get(`${environment.api_produto}/produto`,
      { headers: httpHeaders }).pipe(map(response => <ProdutoModel[]>response));
  }

  save(obj: ProdutoModel) {
    let httpHeaders = new HttpHeaders();
    httpHeaders.set("Content-Type", 'application/json');
    return this.http.post(`${environment.api_produto}/produto`, obj,
      { headers: httpHeaders }).pipe(map(response => <ProdutoModel[]>response));
  }

  delete(id) {
    let httpHeaders = new HttpHeaders();
    return this.http.delete(`${environment.api_produto}/produto/` + id,
      { headers: httpHeaders }).pipe(map(response => <ProdutoModel[]>response));

  }

  findProdutoByBarCode(barCode) {
    let httpHeaders = new HttpHeaders()
    return this.http.get(`${environment.api_item}/item?code=` + barCode,
      { headers: httpHeaders }).pipe(map(response => <ProdutoEstoqueModel[]>response));
  }

  findProdutoByBarCodeMock() {
    return <ProdutoEstoqueModel[]>
      [
        { id: 1, codBarrasProd: 780964423, descProd: 'Lenços umidecidos', pesoProd: 0.300, precoUnit: 10 },

      ]
  }

 
}

