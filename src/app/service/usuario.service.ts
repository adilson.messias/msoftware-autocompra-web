import { Injectable } from '@angular/core';
import { UsuarioModel } from '../model/usuario/usuario.component';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {
  usuario = new Array<UsuarioModel>();
  usuarioDto =  new UsuarioModel;
     
  constructor(private http: HttpClient) { }

  getLogin(): UsuarioModel[] {
    return <UsuarioModel[]>[
    {id: 1, email: 'amessias@gmail.com', senha: '1234',matricula:"7000584"}

    ]
  }

  login(email): Observable<UsuarioModel[]> {
    let httpHeaders = new HttpHeaders();
     return this.http.get(`${environment.api_usuario}?email=`+ email,
      { headers: httpHeaders }).pipe(map(response => <UsuarioModel[]>response));
     }

     getAllMock(): UsuarioModel[] {
      return <UsuarioModel[]>[
        { id: 1, email: 'admin', senha: 'master', matricula:'400065742', perfil:'somente leitura'},
       
      ]
    }
  
    getAll(): Observable<UsuarioModel[]> {
      let httpHeaders = new HttpHeaders();
      return this.http.get(`${environment.api_usuario}/usuario`,
        { headers: httpHeaders }).pipe(map(response => <UsuarioModel[]>response));
    }
  
    save(obj: UsuarioModel) {
      let httpHeaders = new HttpHeaders();
      httpHeaders.set("Content-Type", 'application/json');
      return this.http.post(`${environment.api_usuario}/usuario` , obj ,
        { headers: httpHeaders }).pipe(map(response => <UsuarioModel[]>response));
    }
  
    delete(id) {
      console.log(id)
      let httpHeaders = new HttpHeaders();
      httpHeaders.set("Content-Type", 'application/json');
      return this.http.delete(`${environment.api_usuario}/usuario/` + id, 
        { headers: httpHeaders }).pipe(map(response => <UsuarioModel[]>response));
        
    }
}
