import { Injectable } from '@angular/core';
import { CarrinhoModel } from '../model/carrinho/carrinho.component';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CarrinhoService {
  carrinho = new Array<CarrinhoModel>();
  carrinhoDto = new CarrinhoModel;

  constructor(private http: HttpClient) { }

  saveTable(tableName){
    let httpHeaders = new HttpHeaders();
    httpHeaders.set("Content-Type", 'application/json');
    return this.http.post(`${environment.api_carrinho}/carrinho`, tableName,
      { headers: httpHeaders }).pipe(map(response => <CarrinhoModel[]>response));

  }

  getByCodBarras(table): Observable<CarrinhoModel[]> {
    console.log(table)
    let httpHeaders = new HttpHeaders();
    httpHeaders.set("Content-Type", 'application/json');
    return this.http.get(`${environment.api_carrinho}/carrinho?table=`+ table,
      { headers: httpHeaders }).pipe(map(response => <CarrinhoModel[]>response));
  }

  getAll(): Observable<CarrinhoModel[]> {
    let httpHeaders = new HttpHeaders();
    httpHeaders.set("Content-Type", 'application/json');
    return this.http.options(`${environment.api_carrinho}/carrinho`,
      { headers: httpHeaders }).pipe(map(response => <CarrinhoModel[]>response));
  }

  save(obj){
    let httpHeaders = new HttpHeaders();
    httpHeaders.set("Content-Type", 'application/json');
    return this.http.put(`${environment.api_carrinho}/carrinho`, obj,
      { headers: httpHeaders }).pipe(map(response => <CarrinhoModel[]>response));

  }

  delete(id) {
    let httpHeaders = new HttpHeaders();
    httpHeaders.set("Content-Type", 'application/json');
    return this.http.delete(`${environment.api_carrinho}/carrinho/` + id,
      { headers: httpHeaders }).pipe(map(response => <CarrinhoModel[]>response));

  }


  getAllMock(): CarrinhoModel[] {
    return <CarrinhoModel[]>[
     


    ];
  }

  
}
