import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, } from '@angular/common/http';
import { ClienteModel } from '../model/cliente/cliente.component';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ClienteService {

  constructor(private http: HttpClient) { }

 
  findIdCliente(email , provider, firstName, lastName): Observable<ClienteModel[]> {
     let httpHeaders = new HttpHeaders();
     return this.http.get(`${environment.api_cliente}/cliente?email=`+ email +`&provider=`+ provider +`&firstName=`+ firstName +`&lastName=`+ lastName,
      { headers: httpHeaders }).pipe(map(response => <ClienteModel[]>response));
     }

     verifyClientebyEmail(email , provider): Observable<ClienteModel[]> {
      let httpHeaders = new HttpHeaders();
      return this.http.options(`${environment.api_cliente}/cliente?email=`+ email +`&provider=`+ provider,
       { headers: httpHeaders }).pipe(map(response => <ClienteModel[]>response));
      }
 

    
    getAll(): Observable<ClienteModel[]> {
      let httpHeaders = new HttpHeaders();
      return this.http.get(`${environment.api_cliente}/cliente`,
        { headers: httpHeaders }).pipe(map(response => <ClienteModel[]>response));
    }
  
    save(obj) {
      console.log(" Data : ", obj);
      let httpHeaders = new HttpHeaders();
      httpHeaders.set("Content-Type", 'application/json');
      return this.http.post(`${environment.api_cliente}/cliente` , obj ,
        { headers: httpHeaders }).pipe(map(response => <ClienteModel[]>response));
    }
  
    delete(id) {
      console.log(id)
      let httpHeaders = new HttpHeaders();
      httpHeaders.set("Content-Type", 'application/json');
      return this.http.delete(`${environment.api_cliente}/cliente/` + id, 
        { headers: httpHeaders }).pipe(map(response => <ClienteModel[]>response));
        
    }
}


