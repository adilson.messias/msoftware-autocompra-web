import { Injectable } from '@angular/core';
import { SalesModel } from '../model/sales/sales.component';
import { SaleItensModel } from '../model/sale-Itens/sale.itens.component';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SaleItensService {
  salesDto = new Array<SalesModel>();

  constructor(private http: HttpClient) { }

  getItensByCliente(id, idItem): Observable<SaleItensModel[]> {
   console.log(id , idItem)
    let httpHeaders = new HttpHeaders();
    httpHeaders.set("Content-Type", "Aplication/json");
    return this.http.get(`${environment.api_sales_itens}/salesItem?id=`+id+`&idItens=`+idItem,
      { headers: httpHeaders }).pipe(map(response => <SaleItensModel[]>response));

     
  }

  save(obj, tableName) {
    console.log(">>>>", obj , tableName)
    let httpHeaders = new HttpHeaders();
    httpHeaders.set("Content-Type", "aplication/json");
    return this.http.post(`${environment.api_sales_itens}/salesItem?tableName=`+ tableName , obj ,
      { headers: httpHeaders }).pipe(map(response => <SaleItensModel[]>response));
  }
 
}



