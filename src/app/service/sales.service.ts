import { Injectable } from '@angular/core';
import { SalesModel } from '../model/sales/sales.component';
import { Observable } from 'rxjs';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class SalesService {

  constructor(private http: HttpClient) { }

  getAll(): Observable<SalesModel[]> {
    let httpHeaders = new HttpHeaders();
    return this.http.get(`${environment.api_sales}/vendas`,
      { headers: httpHeaders }).pipe(map(response => <SalesModel[]>response));
  }

  save(obj){
    console.log(obj)
    let httpHeaders = new HttpHeaders();
    httpHeaders.set("Content-Type", 'application/json');
    return this.http.post(`${environment.api_sales}/vendas`, obj,
      { headers: httpHeaders }).pipe(map(response => <SalesModel[]>response));

  }

  getAllMock(): SalesModel[] {
    return <SalesModel[]>[
   
    ]
  }
}
