import { Injectable } from '@angular/core';
import { CarrinhoModel } from '../model/carrinho/carrinho.component';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import { CompraModel } from '../model/carrinho/compra.component';


@Injectable({
  providedIn: 'root'
})
export class CompraService {
  carrinho = new Array<CarrinhoModel>();
  carrinhoDto = new CarrinhoModel;

  constructor(private http: HttpClient) { }

  addItemCarrinho(obj) {
    console.log(">>>",obj)
     let httpHeaders = new HttpHeaders();
    httpHeaders.set("Content-Type", 'application/json');
    return this.http.post(`${environment.api_compra}/compra`, obj,
      { headers: httpHeaders }).pipe(map(response => <CompraModel[]>response));

  }

  getAll(obj): Observable<CompraModel[]> {
    let httpHeaders = new HttpHeaders();
    return this.http.get(`${environment.api_compra}/compra?table=`+ obj,
      { headers: httpHeaders }).pipe(map(response => <CompraModel[]>response));
  }

  deleteItem(obj){
    console.log(obj)
    let httpHeaders = new HttpHeaders();
    httpHeaders.set("Content-Type", 'application/json');
    return this.http.put(`${environment.api_compra}/compra`, obj,
      { headers: httpHeaders }).pipe(map(response => <CompraModel[]>response));

  }


  update(obj){
    console.log(obj)
    let httpHeaders = new HttpHeaders();
    httpHeaders.set("Content-Type", 'application/json');
    return this.http.put(`${environment.api_carrinho}/carrinho`, obj,
      { headers: httpHeaders }).pipe(map(response => <CarrinhoModel[]>response));

  }
  
}
