import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import { PagSeguroItemModel } from '../model/pag-seguro/pagseguro.item.component';

@Injectable({
  providedIn: 'root'
})
export class PagseguroService {

  constructor(private http: HttpClient) { }


  pagSeguro(nome, email, ddd, fone, obj) {
    let httpHeaders = new HttpHeaders();
    httpHeaders.set("Content-Type", 'application/json');
    return this.http.put(`${environment.api_pagseguro}/pagseguro?DDD=`+ddd+`&email=`+email+`&fone=`+fone+`&nome=`+nome,  obj,
      { headers: httpHeaders }).pipe(map(response => <string>response));
  }


}
