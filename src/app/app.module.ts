import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { CarrinhoComponent } from './components/carrinho/carrinho.component';
import { HomeComponent } from './components/home/home.component';
import { ReactiveFormsModule } from '@angular/forms';
import { CustomMaterialModule } from './core/material.module';
import {FormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { ProdutoComponent } from './components/produto/produto.component';
import { DialogComponent}from './components/produto/dialog/dialog.component';
import { DialogAlertaComponent } from './components/dialog-alerta/dialog-alerta.component'
import { DialogCadastroUserComponent } from './components/usuario/cadastro/dialog-cadastro-user/dialog-cadastro-user.component'
import { ConsultaComponent}from './components/usuario/consulta.component';
import { SalesComponent } from './components/sales/sales.component';
import { SaleItensComponent } from './components/sale-itens/sale-itens.component';
import { SaleMonitorComponent } from './components/sale-monitor/sale-monitor.component';
import { CompraComponent } from './components/compra/compra.component';
import { AppInfoDialogComponent } from './components/compra/app-info-dialog/app-info-dialog.component';
import { FormatsDialogComponent } from './components/compra/formats-dialog/formats-dialog.component';
import { DialogCarrinhoComponent } from './components/carrinho/dialog-carrinho/dialog-carrinho.component';
import { ZXingScannerModule } from './components/compra/public_api';
import { ItensCarrinhoComponent } from './components/compra/itens-carrinho/itens-carrinho.component';
import { ClienteComponent } from './components/cliente/cliente.component';
import { PageBemVindoComponent } from './components/cliente/page-bem-vindo/page-bem-vindo.component';
import {SocialLoginModule, AuthServiceConfig, GoogleLoginProvider,FacebookLoginProvider,} from "angularx-social-login";
import { CadastroClienteComponent } from './components/cliente/cadastro-cliente/cadastro-cliente.component';


export function getAuthServiceConfigs() {
  let config = new AuthServiceConfig(
      [
        {
          id: FacebookLoginProvider.PROVIDER_ID,
          provider: new FacebookLoginProvider("786368078470557")
        },
        {
          id: GoogleLoginProvider.PROVIDER_ID,
          provider: new GoogleLoginProvider("1016994148249-cvijokf575kprvsj6899n1ms5a213uu4.apps.googleusercontent.com")
        },
      ]
  );
  return config;
}


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    CarrinhoComponent,
    HomeComponent,
    ProdutoComponent,
    DialogComponent,
    DialogAlertaComponent,
    DialogCadastroUserComponent,
    ConsultaComponent,
    SalesComponent,
    SaleItensComponent,
    SaleMonitorComponent,
    CompraComponent,
    AppInfoDialogComponent,
    FormatsDialogComponent,
    DialogCarrinhoComponent,
    ItensCarrinhoComponent,
    ClienteComponent,
    PageBemVindoComponent,
    CadastroClienteComponent,
      
                
  ],
  
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    BrowserModule,
    BrowserAnimationsModule,
    CustomMaterialModule,
    HttpClientModule,
    FormsModule,
    ZXingScannerModule,
    SocialLoginModule
     
  ],
  entryComponents: [DialogCarrinhoComponent, DialogComponent,DialogAlertaComponent,DialogCadastroUserComponent],
  providers: [
    {
        provide: AuthServiceConfig,
        useFactory: getAuthServiceConfigs
      }
    ],
  bootstrap: [AppComponent,DialogComponent,DialogAlertaComponent,DialogCadastroUserComponent,ClienteComponent]
})
export class AppModule { }
