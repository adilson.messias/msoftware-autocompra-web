import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { HomeComponent } from './components/home/home.component';
import { ProdutoComponent } from './components/produto/produto.component';
import { ConsultaComponent } from './components/usuario/consulta.component';
import { SalesComponent } from './components/sales/sales.component';
import { SaleItensComponent } from './components/sale-itens/sale-itens.component';
import { SaleMonitorComponent } from './components/sale-monitor/sale-monitor.component';
import { CarrinhoComponent } from './components/carrinho/carrinho.component';
import { CompraComponent } from './components/compra/compra.component';
import { ItensCarrinhoComponent } from './components/compra/itens-carrinho/itens-carrinho.component';
import { ClienteComponent } from './components/cliente/cliente.component';
import { PageBemVindoComponent } from './components/cliente/page-bem-vindo/page-bem-vindo.component';
import { CadastroClienteComponent } from './components/cliente/cadastro-cliente/cadastro-cliente.component';


const routes: Routes = [
{path: 'login', component: LoginComponent },
{path: 'home', component: HomeComponent },
{path: 'produto', component: ProdutoComponent },
{path: 'consulta', component: ConsultaComponent },
{path: 'sales', component: SalesComponent },
{path: 'sale-itens', component: SaleItensComponent },
{path: 'sale-monitor', component: SaleMonitorComponent },
{path: 'carrinho', component: CarrinhoComponent },
{path: 'compra', component: CompraComponent },
{path: 'itens-carrinho', component: ItensCarrinhoComponent },
{path: 'cliente', component: ClienteComponent },
{path: 'page-bem-vindo', component: PageBemVindoComponent },
{path: 'cadastro-cliente', component: CadastroClienteComponent },






];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
