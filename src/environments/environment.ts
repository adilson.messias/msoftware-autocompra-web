// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,

  api_usuario:             'http://localhost:8080/autocompra',
  api_produto:             'http://localhost:8080/autocompra',
  api_sales  :             'http://localhost:8080/autocompra',
  api_sales_itens :        'http://localhost:8080/autocompra',
  api_carrinho :           'http://localhost:8080/autocompra',
  api_compra :             'http://localhost:8080/autocompra',
  api_item:                'http://localhost:8080/autocompra',
  api_monitor:             'http://localhost:8080/autocompra',
  api_cliente:             'http://localhost:8080/autocompra', 
  api_pagseguro:           'http://localhost:8080/autocompra', 
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
